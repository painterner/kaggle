# This Python 3 environment comes with many helpful analytics libraries installed
# It is defined by the kaggle/python docker image: https://github.com/kaggle/docker-python
# For example, here's several helpful packages to load in 
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np # linear algebra
import pandas as pd # data processing, CSV file I/O (e.g. pd.read_csv)

# Input data files are available in the "../input/" directory.
# For example, running this (by clicking run or pressing Shift+Enter) will list the files in the input directory

import os
import sys
print(os.listdir("../input"))
__env_test_tmp__ = 'hello'
print(sys.version)

# Any results you write to the current directory are saved as output.
## csv test
if False:
    csvf = pd.DataFrame({'col1': [1, 2,3], 'col2': [3, 4,5]})
    type(csvf)
    csvf
    csvf.columns ## header
    csvf.axes  ## see range
    csvf.T
    csvf.at[2,'col1'] ## point select
    print('csvf.loc[0:10]\n',csvf.loc[0:10])
    print("csvf[0:]['col2']\n",csvf[0:]['col2'])
    print('csvf[0:1]\n',csvf[0:1])                  ### ???
    csvf.loc[0]     ## row select  see also:  csvf[0:1]
    csvf['col1']    ## colum select
    csvf.iloc[:,0]  ## column select by integer
    csvf.loc[[False,True,False]]
    csvf['col1'][[False,True,False]]
    csvf['col1'][csvf['col1'] == 2]
    csvf[0:]['col1'].apply(lambda x: np.sqrt(x))
    print(csvf[0:]['col1'])
        
    ## dataset test
    csvf = pd.read_csv("../input/train.csv")
    al = len(csvf)
    csvf.columns
    csvf.loc[0]['question_text']   ## csvf[0:1]['question_text'] is uncompleted
    csvf['target'][0] == 0
    l = len ( csvf[csvf['target'] == 1])
    l / al *100
    maxl = csvf['question_text'].apply(lambda x: len(x.split()))
    max(maxl)


## start to work
import tensorflow as tf
print(tf.__version__)
import numpy as np
import scipy.stats as st
import pickle

from termcolor import colored
from tqdm import tqdm
import time
need_tk = False
try:
    from matplotlib import pyplot as plt
except:
    need_tk = True   ## for GPU detect
    
    
if True:
    test_output_dir = './'
    tf.app.flags.DEFINE_string('f', '', 'kernel')  ## why we must add this ??
    tf.flags.DEFINE_string("restore", "", "restore name.")
    tf.flags.DEFINE_string("mode", "train", "mode.")
    tf.flags.DEFINE_string('test_output',test_output_dir+'submission.csv','test dataset output file')
    if not os.path.isdir(test_output_dir):
        os.mkdir(test_output_dir)
    FLAGS = tf.flags.FLAGS

class __print_helper():
    """ """
    def __init__(self):
        self.tensor = {}
    def tensor_add(self,name,tensor):
        """ for using by exteranal session """
        self.tensor[name] = tensor
    def get_tensor(self):
        return self.tensor

print_helper = __print_helper()
def model(datas,params):
    """ """ 
    pred = tf.Variable(tf.constant([1.,2]))
    features = datas['phrase']
    label = datas['label']
    word_vector = params.word_vector
    other = datas['other']
        
    def embedding_lookup(input_ids, initialization, word_embedding_name="word_embeddings"):
        # if input_ids.shape.ndims == 2:
        #     input_ids = tf.expand_dims(input_ids, axis=[-1])
        embedding_table = tf.Variable(initialization,name=word_embedding_name)
        output = tf.nn.embedding_lookup(embedding_table, input_ids)
        print("embedding output shape", output.shape)
        return (output, embedding_table)
    def postion_embedding(): ## every word has n embedding, n is the seq length, 
                             ## so weight shape is [vocab_size,length,embedding_dim] ?
        pass

    ## to do: bert net analyse
    ##        bayes frequenty stastic
    ##        analyse the hidden layer (vs analyse neuro workflow in life body, so it's meaningful)
    def gru(x):
        print('rnn input',x)
        assert len(x.shape.as_list()) == 3
        grucell = tf.nn.rnn_cell.GRUCell(params.emb_dims,kernel_initializer= tf.random_uniform_initializer)
        zero_state = grucell.zero_state(other['batch_size'],dtype=tf.float64)  ## the shape of input placeholder for inferring should be [], not [None]
        rnn = tf.nn.dynamic_rnn(grucell,x,initial_state=zero_state)
        print('rnn output shape',rnn[0].shape)
        return rnn
    def bi_gru(x):
        print('rnn input',x)
        assert len(x.shape.as_list()) == 3
        grucell = tf.nn.rnn_cell.GRUCell(params.emb_dims,kernel_initializer= tf.random_uniform_initializer)
        zero_state = grucell.zero_state(other['batch_size'],dtype=tf.float64)
        rnn = tf.nn.dynamic_rnn(grucell,x,initial_state=zero_state)
        print('rnn output shape',rnn[0].shape)
        return rnn
    def mid2terminate_gru(x):
        pass
    def transform_gru(x):
        pass

    ## To do:
    def truncated_np_normal():
        pass
    embedding_initialization = word_vector
    features,embedding_table = embedding_lookup(features,embedding_initialization)
    
    rnnnet = gru(features)
    atten = tf.layers.dense(rnnnet[0],units=150,name='attention/0',kernel_initializer=tf.random_uniform_initializer)
    atten = tf.nn.sigmoid(atten) #
    atten = tf.layers.dense(atten,units=1,name='attention/1',kernel_initializer=tf.random_uniform_initializer)
    atten = tf.nn.softmax(atten,-2)
    rnnnet = tf.reduce_sum(rnnnet[0]*atten,-2)
    print('rnnnet shape',rnnnet.shape)
    output = tf.layers.dense(rnnnet,units=2,name='output',kernel_initializer=tf.random_uniform_initializer)  ## ouput probability ?
    output = tf.nn.softmax(output,-1)
    
    logits = output
    pred = logits
    one_hot_label = tf.one_hot(label,2,dtype=tf.float64)
    loss = tf.reduce_sum( -one_hot_label*tf.log(logits), -1 )  ## pos0 --> good question  pos1 --> bad question
    loss = tf.reduce_mean(loss)
    
    tf.summary.scalar('loss',loss)
    other = {'print':[]}
    print_helper.tensor_add('prediction',pred)
    return pred, loss, other

class C_Dataset_pt():
    """ Dataset base class """
    def get_raw_data(self,mode):
        """ mode: train, eval, all """
        raise NotImplementedError
    def get_numpy_data(self,mode):
        raise NotImplementedError
    def plot_data(self,mode):
        raise NotImplementedError
    def split(self,data,mode):
        raise NotImplementedError
class C_pad():
    pass

class C_Dataset(C_Dataset_pt):
    def __init__(self,params):
        self.params = params
        if not (os.path.isfile('./data/word2vector.pickle') and os.path.isfile('./data/word2id.pickle')):
            train_word = {}
            csvf = pd.read_csv('../input/train.csv')
            def hanleelement(x):
                for v in x.split():
                    train_word[v] = True
                return 0
            csvf['question_text'].apply(lambda x: hanleelement(x))
            for v in list(train_word.keys())[0:10]:
                print(v)
            
            if not os.path.isdir('./data'):
                os.mkdir('./data')
            f = open('../input/embeddings/wiki-news-300d-1M/wiki-news-300d-1M.vec')  ## can we use pandas to handle this ?
            head = next(f)
            word2vector = {}; word2id = {C_pad():0}  ## all sample will be padded to the same length
            l = 0
            for _ in f:
                l += 1
            f.seek(0)
            next(f)
            for _ in tqdm(range(l)):
                m=next(f)
                m = m.split()
                assert len(m) == params.emb_dims+1  ## 301
                if (m[0] in train_word) and (m[0] not in word2vector) or m[0] == 'UNK':
                    word2vector[m[0]] = np.array([float(v) for v in m[1:]])
                    word2id[m[0]] = len(word2id)  ## why this length is 999994 rather than 999995 ? Hypothesis: there are  already UNK in word vector, so when
                                                  ## we extract all vector, we don't need word2id = {"UNK":0}, but it's too large, tensorflow only support <=2GB tensor
            print('head',head,'len of word2id',len(word2id),'len of word2vector', len(word2vector))
            with open('./data/word2vector.pickle','wb') as f:
                pickle.dump(word2vector,f)
            with open('./data/word2id.pickle','wb') as f:
                pickle.dump(word2id,f)
                
        with open('./data/word2vector.pickle','rb') as f:
            word2vector = pickle.load(f)
        with open('./data/word2id.pickle','rb') as f:
            word2id = pickle.load(f)
                
        embedding = np.random.normal(loc=0.,scale=0.02,size=[len(word2id),params.emb_dims])
        for word in word2id:
            if not isinstance(word,C_pad):
                embedding[ word2id[word] ] = word2vector[ word ]
        self.params['word_vector'] = embedding
        print('word2vector length, ,',len(word2vector),word2vector[','])
        print('word2id length, ,',len(word2id),word2id[','])
        print('word2id UNK id', word2id['UNK'])

        def tokenizer_pad(x,padlen):
            list_ = []
            for v in x.split():
                if v in word2id:
                    list_.append(word2id[v] )
                else:
                    list_.append(word2id['UNK'])
            return np.pad(np.array(list_),[0,padlen-len(list_)],'constant')

        csvf = pd.read_csv('../input/train.csv')
        predsetlength = int(self.params.pred_proportion*len(csvf))
        maxl = int( (len(csvf)-predsetlength) * self.params.partial_dataset )
        csvf.loc[0:maxl-1,'question_text'] = csvf.loc[0:maxl-1]['question_text'].apply(lambda x: tokenizer_pad(x,self.params.seq_length))
        self.train_data=csvf.loc[0:maxl-1].values

        evalsetlength = int(self.params.eval_proportion*len(csvf))   ## 1/20 to evaldataset
        assert evalsetlength <= predsetlength
        predpos = int(len(csvf)-predsetlength)
        csvf.loc[predpos:,'question_text'] = csvf.loc[predpos:]['question_text'].apply(lambda x: tokenizer_pad(x,self.params.seq_length))
        self.pred_data=csvf.loc[predpos:].values
        self.eval_data=csvf.loc[predpos:predpos+evalsetlength-1].values
        
        csvf = pd.read_csv('../input/test.csv')
        maxl = len(csvf)
        csvf.loc[0:maxl,'question_text'] = csvf.loc[0:maxl]['question_text'].apply(lambda x: tokenizer_pad(x,self.params.seq_length))
        self.test_data = csvf.loc[0:maxl].values
        print('pred length', len(self.pred_data),'eval length',len(self.eval_data),'train length',len(self.train_data), 'test length',len(self.test_data))
        
        self.dataset_dict = {'train':self.train_data,'pred':self.pred_data,'eval':self.eval_data,"test":self.test_data}
    def get_data_length(self,mode):
        return len(self.dataset_dict[mode])
    def get_numpy_data(self,mode):
        if mode == 'train':
            np.random.shuffle(self.train_data)
            qid = self.train_data[:,0]
            phrase = np.array([v for v in self.train_data[:,1:-1][:,0]])     ## is a list like [[1, array([2, 3]), 2], [0, array([1, 2]), 1]], so we can shuffle it use np.random.shuffle
            label = np.array(self.train_data[:,-1])                          ## why we must use v for v to extract it then concatenate it ??
            return qid, phrase, label
        elif mode == 'eval':
            qid = self.eval_data[:,0]
            phrase = np.array([v for v in self.eval_data[:,1:-1][:,0]])
            label = np.array(self.eval_data[:,-1])
            return qid, phrase, label
        elif mode == 'pred':
            qid = self.pred_data[:,0]
            phrase = np.array([v for v in self.pred_data[:,1:-1][:,0]])
            label = np.array(self.pred_data[:,-1])
            return qid, phrase, label
        elif mode == 'test':
            qid = self.test_data[:,0]
            phrase = np.array([v for v in self.test_data[:,1:][:,0]])
            fakerlabel = np.repeat([-987],len(phrase))
            return qid, phrase, fakerlabel
        else:
            raise Exception('mode is error')

class C_SummaryManager():
    def __init__(self,outdir):
        self.outdir = outdir
        self.summary_writer = None
        self.passthrough = True

    def set_session(self,sess):
        if self.passthrough: return None
        self.summary_writer = tf.summary.FileWriter(self.outdir,sess.graph)
    def get_merged(self):
        if self.passthrough: return []
        if self.summary_writer is None:
            raise Exception("error")
        merged = tf.summary.merge_all()
        return merged
    def add_summary(self,summary,step):
        if self.passthrough: return None
        self.summary_writer.add_summary(summary,step)
    def close(self):
        if self.passthrough: return None
        self.summary_writer.close()
    def flush(self):
        if self.passthrough: return None
        self.summary_writer.flush()

class ClockTogger():
    class IC():
        def __init__(self,perid,warn):
            self.perid = perid
            self.count = [0 for _ in range(len(perid))]
            self.warn = warn

    def __init__(self):
        self.clocks = {}
        self.clocks_group = { } ## To do: add clocks_group to contain clocks
        self.isstart = True

    def add(self,perid,name,overwarn=False):
        assert isinstance(perid,(tuple,list))
        assert name not in self.clocks
        perid = tuple(perid)
        ic = self.IC(perid,overwarn)
        self.clocks[name] = ic
        
    def start(self):
        self.isstart = True
    def stop(self):
        self.isstart = False

    # @property
    def get_clock(self,name):
        return tuple(self.clocks[name].count)

    def run(self,key=None):
        if not self.isstart:
            return
        if key is not None:
            if isinstance(key,str):
                clocks = [key]
            else:
                clocks = key
        else:
            clocks = list(self.clocks.keys())
        for clock in clocks:
            perid = self.clocks[clock].perid
            count = self.clocks[clock].count
            warn = self.clocks[clock].warn
            l = len(perid)
            count[-1] += 1
            for i in range(l):
                pos = -(i+1)
                if count[pos] == perid[pos]:
                    count[pos] = 0
                    if i < l-1:
                        count[pos+1] += 1
                    elif warn:
                        print('ClockTogger overflow, clock name is',clock)

class C_Hyparams(object):
    def __init__(self,v):
        assert isinstance(v,dict)
        for name in v:
            super(C_Hyparams,self).__setattr__(name,v[name])
    def __getitem__(self,key):  ## c[]
        return super(C_Hyparams,self).__getattribute__(key)
    def __setitem__(self,name,v):
        super(C_Hyparams,self).__setattr__(name,v)
    def __getattribute__(self,key): ## c.p      
        return super(C_Hyparams,self).__getattribute__(key) 
    def __setattr__(self,name,key): ## c.p = a  
        raise  NotImplementedError

class C_DL():
    ## to Flags   batch_size, train mode, partial_dataset, 
    hyparams = C_Hyparams({"batch_size":128, "emb_dims": 300, "seq_length": 150,'mode':FLAGS.mode,
                ## the different between in pred and eval is that pred is used in command line
                "train": "train", "pred":"pred", "test":"test",     ## don't use eval ,eval only use in progress of training
                ## 1.3million: 0.01: 13000 tain, 0.005 6500 eval(if speed is fast, we can use 0.05) --> 3min eval in gpu ?
                ## 80000 bad: 0.01: 800 train,  0.005: 400 eval
                'partial_dataset': 0.1, 'eval_proportion': 0.005, 'pred_proportion': 0.05,
                'shuffle_period':1000, 'train_epoch': 2,
                "optimizer": tf.train.AdamOptimizer,"learning_rate": 1e-3,"decay_rate":0.96,"decay_steps": 1000,})
    
    cdata = C_Dataset(hyparams)
    hyparams['training_eval_peroid'] = cdata.get_data_length('train') // hyparams.batch_size // 10

    clockTogger = ClockTogger()
    clockTogger.add([hyparams.shuffle_period],'switch')
    clockTogger.add([hyparams.training_eval_peroid],'eval')
    def schechle(self,mode):
        self.clockTogger.run() ## run it every iter
        return 10 if self.clockTogger.get_clock(mode)[0] == 0 else 20


    def prepare(self,mode):
        assert mode in ['pred','eval','test','train']
        datas = self.cdata.get_numpy_data(mode)
        index = 0
        print('prepare',mode)
        while True: 
            # if mode == 'eval':
            #     print(len(datas[1]))
            if index >= len(datas[1]):
                if mode == 'eval' or mode == 'test' or mode == 'pred':
                    break
                index = 0
                
            if  mode=='train':  ## for shuffling data periodly
                self.clockTogger.run('switch')
                time = self.clockTogger.get_clock('switch')
                if time[-1] == 0:
                    datas = self.cdata.get_numpy_data(mode) 
                    
            qid = datas[0][index:self.hyparams.batch_size+index]
            phrase = datas[1][index:self.hyparams.batch_size+index]
            label = datas[2][index:self.hyparams.batch_size+index]

            index += self.hyparams.batch_size
            yield qid, phrase,label

    def model(self,inputs):
        o = model(inputs,self.hyparams)
        return o
        
    def optimizer(self,**kargs):
        global_step,loss = (kargs['global_step'],kargs['loss'])
        learning_rate = tf.train.exponential_decay(self.hyparams['learning_rate'], global_step,
                                                self.hyparams['decay_steps'], self.hyparams['decay_rate'], staircase=True) ## staircase=True: the v of 0.96**v is a integer
        return self.hyparams.optimizer(learning_rate).minimize(loss)
    def saver_process(self,*args, **kwargs):
        sess,restore_point = (kwargs['sess'],kwargs['restore_point'])
        if FLAGS.restore:
            print(colored("restore model...",color='blue'))
            variables = tf.global_variables()
            variables_to_restore = [v for v in variables if (v.name.split('/')[0]!='boosting')] 
            saver = tf.train.Saver(variables_to_restore)
            saver.restore(sess,restore_point )
        saver = tf.train.Saver()
        return saver

    def placeholder(self):
        inp = tf.placeholder(dtype=tf.int64,shape=[None,self.hyparams.seq_length]) ## ids is int -> embedding is float
        label = tf.placeholder(dtype=tf.int64,shape=[None])
        other = {'batch_size': tf.placeholder(dtype=tf.int32,shape=[]),
                }
        return inp,label,other
        
    def train(self):
        summary = C_SummaryManager('./summary/')
        inp,label,other_placeholder = self.placeholder()
        model_out = self.model({'phrase':inp,'label':label,'other':other_placeholder})

        global_step = tf.Variable(0, trainable=False)
        global_step_ops = tf.assign_add(global_step,1)
        out = self.optimizer(loss= model_out[1],global_step=global_step)

        ## don't try to clarify the max_train_iteror, it's doesn't matter, so don't waste time on this
        max_train_iteror = self.cdata.get_data_length('train') // self.hyparams.batch_size * self.hyparams.train_epoch 
        all_predboolean = 0
        all_count = 0

        # datagenerator = self.prepare(FLAGS.mode)
        # if FLAGS.mode == 'train':
        #     evaldatagenerator = self.prepare('eval')

        with tf.Session() as sess:
            summary.set_session(sess)
            sess.run(tf.global_variables_initializer())
            saver = self.saver_process(sess=sess,restore_point= "./checkpoint/"+FLAGS.restore)

            train_op = out if FLAGS.mode == 'train' else []

            assert FLAGS.mode in ['train','pred','test']
            if FLAGS.mode == 'train':
                data_generator = self.prepare(FLAGS.mode)
            self.current_mode = FLAGS.mode; self.next_mode = FLAGS.mode

            timeA = time.time()
            while True:
                self.current_mode = self.next_mode
                if self.current_mode != 'train':
                    other_step = 0
                if self.current_mode in ['pred','test']:
                    if self.current_mode in ['test']:
                        ftest = open(FLAGS.test_output,'w')
                        ftest.write('qid,prediction\n')
                        ftestcount = 0
                    d_generator = self.prepare(self.current_mode)
                elif self.current_mode == 'train':
                    d_generator = data_generator
                    self.next_mode = 'eval'; 
                elif self.current_mode == 'eval':
                    d_generator = self.prepare('eval')
                    self.next_mode = 'train'; 
                else:
                    raise Exception('error')
                    
                for data in d_generator:
                    feed_dict = {inp:data[1],label:data[2],other_placeholder['batch_size']:data[0].shape[0]}
                    merged = []
                    if self.current_mode == 'train':
                        train_op = out
                        step_value = (global_step_ops).eval()
                        if max_train_iteror < step_value:
                            self.next_mode = 'test'
                            break
                    else:
                        train_op = []
                        other_step += 1; step_value = other_step

                    if step_value  % 1 ==  0:
                        if self.current_mode=='train':
                            summary.passthrough == True
                            if step_value % 100 == 0:
                                summary.passthrough == True
                        ## To do: build a new summary for other mode
                        else:
                            summary.passthrough = True
                        merged = summary.get_merged()

                        r  = sess.run([model_out[0],model_out[1],model_out[2],train_op,
                                        merged,print_helper.get_tensor()],feed_dict=feed_dict)
                        pred,loss,other,  _,merged, tensor = r

                        predresult = np.argmax(pred,-1)
                        all_predboolean += np.sum( np.asarray( np.equal(predresult,feed_dict[label]),dtype=np.int64) )
                        all_count += len(pred)

                        summary.add_summary(merged,step_value)
                        summary.flush()
                        
                    # if step_value % self.hyparams['training_eval_peroid'] == 0:
                    print_peroid_ = 100
                    if step_value % print_peroid_ == 0:
                        timeB = time.time()
                        rate = print_peroid_*self.hyparams.batch_size / (timeB-timeA)
                        if other['print'] == []:
                            print(self.current_mode,"sample/sec",rate,"pred",pred[0],"loss",loss,'other',tensor['prediction'][0])
                        timeA = time.time()

                    if True:
                        if self.current_mode == 'train':
                            if step_value % 1000 == 0:
                                print('save model') 
                                # saver.save(sess,"./checkpoint/model"+str(global_step.eval()+1)+".ckpt")
                                saver.save(sess,"./checkpoint/modeltest.ckpt")
                
                    # print(self.current_mode,self.next_mode)
                    if self.current_mode == 'train':
                        self.clockTogger.run('eval')
                        if self.clockTogger.get_clock('eval')[-1] == 0:
                            break

                    if self.current_mode == 'test':
                        for qid_, pred_ in zip(data[0],np.argmax(pred,-1)):
                            ftest.write(str(qid_)+','+str(pred_)+'\n')
                            ftestcount += 1

                if self.current_mode in ['eval','pred']:
                    acc = all_predboolean / all_count
                    all_predboolean = 0
                    all_count = 0
                    print('acc',acc)
                    if self.next_mode == 'pred':
                        break
                elif self.current_mode in ['test']:
                    break
            if self.current_mode == 'test':
                assert ftestcount == 56370
                ftest.close()

def test_data_generate():
    c = C_DL()
    def test(mode):
        data = c.prepare(mode)
        for i,v in enumerate(data):
            if i > 3:
                break
            print(mode, v[1] )
    test('train')
    test('eval')
    test('test')
    
def main(_):
    # sub = pd.read_csv("../input/sample_submission.csv")
    # sub.to_csv('./submission.csv',index=False)
    if False:
        test_data_generate()
    else:
        c =C_DL()
        timeA = time.time()
        c.train()
        timeB = time.time()
        print("completed, time",timeB-timeA)

if __name__ == "__main__":
    tf.app.run()
    

"""
constraints:
    must commit a kernel from scratch (include pre-processing, training, prediction)
To do:
    train and simulatinously think the method of processing unbalance data
    set fixed random seed

speed:
    batch_size: 20 -> 32samples/s
                128 -> 172 sample/s
"""