import time
class ClockTogger():
    class IC():
        def __init__(self,perid,warn):
            self.perid = perid
            self.count = [0 for _ in range(len(perid))]
            self.warn = warn

    def __init__(self):
        self.clocks = {}
        self.clocks_group = { } ## To do: add clocks_group to contain clocks
        self.isstart = True

    def add(self,perid,name,overwarn=False):
        assert isinstance(perid,(tuple,list))
        assert name not in self.clocks
        perid = tuple(perid)
        ic = self.IC(perid,overwarn)
        self.clocks[name] = ic
        
    def start(self):
        self.isstart = True
    def stop(self):
        self.isstart = False

    # @property
    def get_clock(self,name):
        return tuple(self.clocks[name].count)

    def run(self,key=None):
        if not self.isstart:
            return
        if key is not None:
            if isinstance(key,str):
                clocks = [key]
            else:
                clocks = key
        else:
            clocks = list(self.clocks.keys())
        for clock in clocks:
            perid = self.clocks[clock].perid
            count = self.clocks[clock].count
            warn = self.clocks[clock].warn
            l = len(perid)
            count[-1] += 1
            for i in range(l):
                pos = -(i+1)
                if count[pos] == perid[pos]:
                    count[pos] = 0
                    if i < l-1:
                        count[pos+1] += 1
                    elif warn:
                        print('ClockTogger overflow, clock name is',clock)

clock = ClockTogger()
clock.add([5],'a')
clock.add([3],'b')
while True:
    clock.run('a')
    if clock.get_clock('a')[-1] == 0:
        print('a',0)
    else:
        print('a',1)

    clock.run('b')
    if clock.get_clock('b')[-1] == 0:
        print('b',0)
    else:
        print('b',1)

    time.sleep(0.5)