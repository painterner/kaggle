## Dev Tools test command
GET _search
{
  "query": {
    "match_all": {}
  }
}

PUT /library/books/1
{
  "title": "Norwegian Wood",
  "name": {
    "first": "Haruki",
    "last": "Murakami"
  },
  "publish_date": "1987-09-04T00:00:00+0900",
  "price": 19.95
}


GET /library/books/1

POST /library/books/
{
  "title": "Kafka on the Shore",
  "name": {
    "first": "Haruki",
    "last": "Murakami"
  },
  "publish_date": "2002-09-12T00:00:00+0900",
  "price": 19.95
}

GET /library/books/Ns8Fk2gBwULIFb8vZd5l

## update
PUT /library/books/1
{
  "title": "Norwegian Wood",
  "name": {
    "first": "Haruki",
    "last": "Murakami"
  },
  "publish_date": "1987-09-04T00:00:00+0900",
  "price": 29.95
}

GET /library/books/1

## update
POST /library/books/1/_update
{
  "doc": {
    "price": 10
  }
}


## add
POST /library/books/1/_update
{
  "doc": {
    "price_jpy": 1800
  }
}

## delete
DELETE /library/books/1
## delete
DELETE /library


## use bulk to create
POST /library/books/_bulk
{"index": {"_id": 1}}
{"title": "The quick brown fox", "price": 5}
{"index": {"_id": 2}}
{"title": "The quick brown fox jumps over the lazy dog", "price": 15}
{"index": {"_id": 3}}
{"title": "The quick brown fox jumps over the quick dog", "price": 8}
{"index": {"_id": 4}}
{"title": "Brown fox and brown dog", "price": 2}
{"index": {"_id": 5}}
{"title": "Lazy dog", "price": 9}

## search
GET /library/books/_search?size=5

## search word
GET /library/books/_search
{
  "query": {
    "match": {
      "title": "fox"
    }
  }
}

## search with or
GET /library/books/_search
{
  "query": {
    "match": {
      "title": "quick dog"
    }
  }
}

## phrase with whitespace
GET /library/books/_search
{
  "query": {
    "match_phrase": {
      "title": "quick dog"
    }
  }
}

## score ?
GET /library/books/_search?explain
{
  "query": {
    "match": {
      "title": "quick"
    }
  }
}

