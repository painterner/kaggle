__background__
all are belong to classify decision problems

## keyword
verified  验证

## ??
__wiki_NP-hardness__
Awkwardly, it does `not restrict` the class NP-hard to decision problems, for instance it also includes search problems, or optimization problems. 
__P versus NP problem__
that if so, then the discovery of mathematical proofs could be automated. 

##Problem sets
Sudoku

##unproven ( but some has most likely result)
likely: scientists suspect that `none` of these problems can be solved `quickly`


## To do:
__Nondeterministic algorithm__
nondeterministic finite automaton.

__NP Problem__
nondeterministic polynomial time

__NP-completeness__
 Researchers have shown that many of the problems in NP have the extra property that `a` fast solution to `any` one of them could be used to build a quick solution to any other problem in NP, a property called NP-completeness

__NP-hard__
subset sum problem

__P Problem__
deterministic polynomial time
In this theory, the class P consists of all those decision problems (defined below) that can be solved on a deterministic sequential machine in an amount of time that is polynomial in the size of the input; 

## P versus NP problem
The P versus NP problem is a major unsolved problem in computer science. It asks whether every problem whose solution can be `quickly verified` (technically, `verified in polynomial time`) can also be `solved quickly` (again, in polynomial time). 

An answer to the `P = NP` question would determine whether problems that can be `verified` in polynomial time, like Sudoku, can `also be solved in` polynomial time. If it turned out that `P ≠ NP`, it would mean that there are problems in NP that are harder to compute than to verify: they could `not be solved in` polynomial time, but the answer could be verified in polynomial time.  
>> it means that  P=NP --> verified == solved
                  P≠NP --> solved > verified , but we can use non-deterministic polynomial time to sove it ?